<?php
$lines = explode("\n", file_get_contents('urls.txt'));
foreach ($lines as $url) {
  if ($url == '') continue;

	$ch = curl_init($url);
	curl_setopt($ch, CURLOPT_HEADER, true);
	curl_setopt($ch, CURLOPT_NOBODY, true);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($ch, CURLOPT_TIMEOUT, 10);
	$output = curl_exec($ch);
	$httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
	curl_close($ch);

	echo "<pre>";
	echo $url . ' → HTTP CODE: ' . $httpcode;
}
