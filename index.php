<?php

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

// Get URLs
$urls = array();
$handle = fopen("urls.txt", "r");
if ($handle) {
    while (($line = fgets($handle)) !== false) {
        $urls []= $line;
    }
    fclose($handle);
} else {
    echo "Problem opening file";
    die();
}

// Test them, store the response code

foreach ($urls as $url) {
	// code...
	echo $url;
	$options['http'] = array(
	    'method' => "HEAD",
	    'ignore_errors' => 1,
	);

	$context = stream_context_create($options);

	$body = file_get_contents($url, NULL, $context);

	$responses = parse_http_response_header($urls);

	$code = $responses[0]['status']['code']; // last status code

	echo "Status code (after all redirects): $code<br>\n";

	$number = count($responses);

	$redirects = $number - 1;

	echo "Number of responses: $number ($redirects Redirect(s))<br>\n";
}
var_dump($urls);
function parse_http_response_header(array $headers)
{
    $responses = array();
    $buffer = NULL;
    foreach ($headers as $header)
    {
        if ('HTTP/' === substr($header, 0, 5))
        {
            // add buffer on top of all responses
            if ($buffer) array_unshift($responses, $buffer);
            $buffer = array();

            list($version, $code, $phrase) = explode(' ', $header, 3) + array('', FALSE, '');

            $buffer['status'] = array(
                'line' => $header,
                'version' => $version,
                'code' => (int) $code,
                'phrase' => $phrase
            );
            $fields = &$buffer['fields'];
            $fields = array();
            continue;
        }
        list($name, $value) = explode(': ', $header, 2) + array('', '');
        // header-names are case insensitive
        $name = strtoupper($name);
        // values of multiple fields with the same name are normalized into
        // a comma separated list (HTTP/1.0+1.1)
        if (isset($fields[$name]))
        {
            $value = $fields[$name].','.$value;
        }
        $fields[$name] = $value;
    }
    unset($fields); // remove reference
    array_unshift($responses, $buffer);

    return $responses;
}
